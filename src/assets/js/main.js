import './../css/main.css';

jQuery(document).ready(function($){
	var app = {

		init: function(){

			this.listeners();
			this.chosen();
			this.datepickers();
			this.tooltip();

		},
		listeners: function(){
			$('.modal').on('shown.bs.modal', function (e) {
				$('input.datepicker').datepicker({
					format: 'dd-mm-yyyy',
					autoHide: true,
					autoPick: true,
					defaultDate: null,
					weekStart: 1
				});

			})
		},
		chosen: function(){
			$(".chosen").chosen();
		},
		datepickers: function(){

			$('input.datepicker').datepicker({
				format: 'dd-mm-yyyy',
				autoHide: true,
				autoPick: false,
				weekStart: 1,
			});

			var im = new Inputmask("99-99-9999",{ "placeholder": "dd-mm-yyyy" });
			im.mask($('input.datepicker'));
		},
		tooltip: function(){
			
			$('[data-toggle="tooltip"]').tooltip(); 

		}

	}
	app.init();
});
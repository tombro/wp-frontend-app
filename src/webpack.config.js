const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const AssetsPlugin = require('assets-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = (env, argv) => ({
	entry: './assets/js/main.js',
	output: {
		filename: 'js/[name].[hash].min.js',
		path: path.resolve(__dirname, 'dist'),
	},
	plugins: [
	new MiniCssExtractPlugin({
		filename: "css/[name].[hash].min.css",
		chunkFilename: "[id].css"
	}),
	new AssetsPlugin({
		path: './dist',
		filename: 'assets.json',
		prettyPrint: true,
		includeAllFileTypes: false,
		fileTypes: ['js', 'css']
	})
	],
	module: {
		rules: [
		{
			test: /\.css$/,
			use: [
			{
				loader: MiniCssExtractPlugin.loader,
				options: {
					publicPath: '../'
				}
			},
			"css-loader"
			]
		},
		{
			test: /\.(jpg|png)$/,
			use: {
				loader: "file-loader",
				options: {
					name: "./images/[name].[ext]",
				},
			},
		},
		]
	},
	optimization: {
		minimizer: argv.mode === 'production' ? [ new OptimizeCSSAssetsPlugin(), new UglifyJsPlugin() ] : []
	},
	externals: {
		jquery: 'jQuery'
	}
});
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
get_header();
global $requested_template;
printf( 'Template %s not found', $requested_template );
get_footer();
?>

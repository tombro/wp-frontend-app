<?php

class FA_Paginator {

	private $limit;
	private $page;
	private $total;

	public function __construct( $numrows, $limit, $page ) {

		$this->total = $numrows;
		$this->limit = $limit;
		$this->page = $page;

	}

	public function createLinks( $url ) {
		if ( $this->limit == -1 ) {
			return '';
		}
		$links = 3;

		$last       = ceil( $this->total / $this->limit );

		$start      = ( ( $this->page - $links ) > 0 ) ? $this->page - $links : 1;
		$end        = ( ( $this->page + $links ) < $last ) ? $this->page + $links : $last;

		$html       = '<ul class="pagination">';

		$class      = ( $this->page == 1 ) ? "disabled" : "";

		$query_args = array(
			'paged' => $this->page - 1
		);
		$new_url = add_query_arg( $query_args, $url );
		$html       .= '<li class="page-item ' . $class . '"><a class="page-link" href="' . $new_url . '">&laquo;</a></li>';

		if ( $start > 1 ) {
			$html   .= '<li class="page-item"><a class="page-link" href="?limit=' . $this->limit . '&page=1">1</a></li>';
			$html   .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
		}

		for ( $i = $start ; $i <= $end; $i++ ) {

			$query_args = array(
				'paged' => $i
			);
			$new_url = add_query_arg( $query_args, $url );
			$class  = ( $this->page == $i ) ? "active" : "";
			$html   .= '<li class="page-item ' . $class . '"><a class="page-link" href="' . $new_url . '">' . $i . '</a></li>';
		}

		if ( $end < $last ) {
			$query_args = array(
				'paged' => $last
			);
			$new_url = add_query_arg( $query_args, $url );
			$html   .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
			$html   .= '<li class="page-item"><a class="page-link" href="' . $new_url . '">' . $last . '</a></li>';
		}

		$query_args = array(
			'paged' => $this->page + 1
		);
		$new_url = add_query_arg( $query_args, $url );
		$class      = ( $this->page == $last ) ? "disabled" : "";
		if( $this->page == $last ){
			$html   .= '<li class="page-item disabled"><a class="page-link">&raquo;</a></li>';
		}
		else{
			$html       .= '<li class="page-item ' . $class . '"><a class="page-link" href="' . $new_url . '">&raquo;</a></li>';
		}

		$html       .= '</ul>';

		return $html;
	}

}
<?php
class FACustomJsonManifest {
	private $manifest;

	public function __construct($manifest_path) {
		if (file_exists($manifest_path)) {
			$this->manifest = json_decode(file_get_contents($manifest_path), true);
		} else {
			$this->manifest = [];
		}
	}

	public function get() {
		return $this->manifest;
	}
}

function fa_asset_path( $filename ){

	$filename = rtrim( ltrim( $filename, '/' ), '/' );
	$distPath = plugins_url('dist/',dirname(dirname(__FILE__)));
	$directory = dirname($filename) . '/';
	$file = basename($filename);

	$fileArray = explode('.', $file);

	static $manifest;

	if (empty($manifest)) {
		$manifest_path = __DIR__ . '/../../dist/assets.json';
		$manifest = new FACustomJsonManifest($manifest_path);
	}
	$themanifest = $manifest->get();
	if (array_key_exists($fileArray[0], $themanifest) && array_key_exists($fileArray[1], $themanifest[$fileArray[0]]) ) {

		return $distPath . $manifest->get()[$fileArray[0]][$fileArray[1]];
	} 
	else {
		return get_theme_file_uri( 'assets/' . $filename );
	}

}
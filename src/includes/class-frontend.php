<?php
class FA_Frontend
{
    public function __construct()
    {
        add_filter('the_content', array( $this, 'show_notices' ), 20);
        add_action('template_redirect', array( $this, 'redirect_front_page' ));
        add_filter('login_redirect', array( $this, 'login_redirect' ), 10, 3);
        add_filter('show_admin_bar', array( $this, 'hide_toolbar' ));
        add_action('fa_after_header', array( $this, 'breadcrumbs' ));
        add_filter('document_title_parts', array( $this,'page_title' ));
    }

    public function hide_toolbar()
    {
        return apply_filters('fa_show_toolbar', false);
    }

    public function show_notices($content)
    {
        if (isset($_GET['notice'])) {
            $message = '';
            switch ($_GET['notice']) {
                case __('updated', fa_text_domain()):
                    $class = 'success';
                    $message = __('Updated', fa_text_domain());
                    break;
                case __('created', fa_text_domain()):
                    $class = 'success';
                    $message = __('Created', fa_text_domain());
                    break;
                case __('forbidden', 'frontend-app'):
                    $class = 'danger';
                    $message = __('You are not authorized to access this page', 'frontend-app');
                    break;
                case __('deleted', fa_text_domain()):
                    $class = 'danger';
                    $message = __('Deleted', fa_text_domain());
                    break;
            }
            ob_start(); ?>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="alert alert-<?php echo $class; ?>" role="alert">
							<?php echo $message; ?>
						</div>
					</div>
				</div>
			</div>
			<?php
            $notice = ob_get_clean();
            return $notice . $content;
        }
        return $content;
    }

    public function login_redirect($redirect_to)
    {
        return home_url();
    }

    public function redirect_front_page()
    {
        if (!is_admin() && strtok($_SERVER['REQUEST_URI'], '?') == '/') {
            if ($home = $this->get_home()) {
                if ($_SERVER['QUERY_STRING']) {
                    $home .= '?' . $_SERVER['QUERY_STRING'];
                }
                wp_redirect(home_url() . '/' . $home, 302);
            }
        }
    }

    private function get_home()
    {
        $home = false;
        foreach (fa_get_config('endpoints') as $key => $endpoint) {
            if (isset($endpoint['is_home']) && $endpoint['is_home']) {
                $home = $endpoint['plural_slug'];
                break;
            }
        }
        return $home;
    }

    public function breadcrumbs()
    {
        if (is_front_page() && apply_filters('fa_breadcrumbs_hide_on_home', true)) {
            return;
        }

        $links = array();
        $title = '';
        $home = apply_filters('fa_breadcrumbs_home', '<i class="fa fa-home"></i> ' . __('Home', fa_text_domain()));
        global $wp_query;

        if (isset($wp_query->query_vars['custompage'])) {
            $pages = fa_get_config('endpoints');
            $page = $pages[$wp_query->query_vars['custompage']];

            if (!isset($wp_query->query_vars['custompage_scope']) && !isset($wp_query->query_vars['custompage_id'])) {
                $links = array(
                    array( 'url' => fa_url(), 'name' => $home )
                );
                $title = $page['plural_label'];
            } elseif (isset($wp_query->query_vars['custompage_scope']) && !isset($wp_query->query_vars['custompage_id'])) {
                $links = array(
                    array( 'url' => fa_url(), 'name' => $home ),
                    array( 'url' => fa_url($page['plural_slug']), 'name' => $page['plural_label'] )
                );
                switch ($wp_query->query_vars['custompage_scope']) {
                    case 'new':
                    $title = apply_filters('fa_breadcrumbs_new_' . $page['slug'] . '_title', sprintf(__('New %s', fa_text_domain()), $page['singular_label']) );
                    break;
                }
            } else {
                $links = apply_filters(
                    'fa_breadcrumbs_edit_' . $wp_query->query_vars['custompage'],
                    array(
                        array( 'url' => fa_url(), 'name' => $home ),
                        array( 'url' => fa_url($page['plural_slug']), 'name' => $page['plural_label'] )
                    )
                );

                $title = apply_filters('fa_breadcrumbs_edit_' . $wp_query->query_vars['custompage'] . '_title', get_the_title($wp_query->query_vars['custompage_id']), $wp_query->query_vars['custompage_id']);
            }
        } elseif (apply_filters('fa_breadcrumbs_show_on_other_pages', true)) {
            $links = array( array( 'url' => fa_url(), 'name' => $home ) );
            $title = get_the_title();
            if (is_front_page()) {
                $title = __('Home', fa_text_domain());
                $links = array();
            }
        } ?>
		<div class="col-12">
			<div class="bg-light p-3 mb-4 rounded">
				<?php if (!empty($links)): ?>
					<?php foreach ($links as $key => $link): ?>
						<a href="<?php echo $link['url']; ?>"><?php echo $link['name']; ?></a> <i class="fa fa-angle-right"></i> 
					<?php endforeach; ?>
				<?php endif; ?>
				<h1 class="mb-0"><?php echo $title; ?></h1>
			</div>
		</div>
		<?php
    }

    public function page_title($title_parts){
    	global $wp_query;
    	if (isset($wp_query->query_vars['custompage'])){
            $pages = fa_get_config('endpoints');
    		$page = $pages[$wp_query->query_vars['custompage']];
    		$title_parts['title'] = $page['plural_label'];
    	}
    	if( isset($wp_query->query_vars['custompage_id']) ){
    		$title_parts['title'] = get_the_title( $wp_query->query_vars['custompage_id'] );
    	}
    	return $title_parts;
    }
}
new FA_Frontend();
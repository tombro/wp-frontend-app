<?php
abstract class FA_Post_Type_Single
{
    protected $ID;
    protected $post_type;

    public function __construct($id = null)
    {
        if ($id) {
            $this->ID = $id;
        }
    }

    public function get($key)
    {
        return get_post_meta($this->get_ID(), $key, true);
    }

    public function set($key, $value)
    {
        return update_post_meta($this->get_ID(), $key, $value);
    }

    public function add_meta($key, $value)
    {
        return add_post_meta($this->get_ID(), $key, $value);
    }

    public function remove_meta($key, $value = null)
    {
        if ($value) {
            delete_post_meta($this->get_ID(), $key, $value);
        } else {
            delete_post_meta($this->get_ID(), $key);
        }
    }

    public function get_ID()
    {
        return $this->ID;
    }

    public function get_post_type()
    {
        return $this->post_type;
    }

    public function get_name()
    {
        return get_the_title($this->get_ID());
    }

    public function get_content()
    {
        $post_object = get_post($this->get_ID());
        return $post_object->post_content;
    }

    public function get_url()
    {
        return home_url() . '/' . $this->get_slug() . '/' . $this->get_ID() . '/' . __('edit', fa_text_domain());
    }

    protected function get_slug()
    {
        $endpoints = fa_get_config('endpoints');

        foreach ($endpoints as $key => $endpoint) {
            if (isset($endpoint['slug']) && $endpoint['slug'] == $this->post_type) {
                return $endpoint['plural_slug'];
            }
        }
    }

    public function upsert($args)
    {
        $args = apply_filters('fa_upsert_' . $this->post_type, $args);
        
        $id 		= (isset($args['id']) ? sanitize_text_field($args['id']) : '');
        $title 		= (isset($args['title']) ? sanitize_text_field($args['title']) : '');
        $content 	= (isset($args['content']) ? sanitize_text_field($args['content']) : '');
        $fields 	= (isset($args['meta']) ? $args['meta'] : array());

        if (!$id || $id == 0) {
            $args = array(
                'post_title' 		=> $title,
                'post_type' 		=> $this->post_type,
                'post_status' 		=> 'publish',
                'posts_per_page' 	=> -1
            );
            if (isset($content)) {
                $args['post_content'] = $content;
            }
            $id = wp_insert_post($args);
        } else {
            $args = array(
                'ID' 			=> $id,
                'post_title' 	=> $title
            );
            if (isset($content)) {
                $args['post_content'] = $content;
            }
            wp_update_post($args);
        }

        foreach ($fields as $key => $value) {
            update_post_meta($id, $key, $value);
        }

        $this->__construct($id);
        return $id;
    }
}

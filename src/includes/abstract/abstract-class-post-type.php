<?php
abstract class FA_Post_Type{
	
	protected static $_instances = null;
	protected $post_type;
	protected $class;

	public static function instance() {

        $class = get_called_class();
        if (!isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class();
        }
        return self::$_instances[$class];
        
	}

	public function get_post_type(){

		return $this->post_type;

	}

	public function get( $all = false ){

		$is_search = get_query_var( 's' );
		$return = array();
		$args = array(
			'post_type' => $this->post_type,
			'posts_per_page' => apply_filters( 'fa_posts_per_page', get_option( 'posts_per_page' ), $this->post_type )
		);
		if( $all ){
			$args['posts_per_page'] = -1;
		}
		$args['paged'] = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		if( $is_search ){
			$args['s'] = get_query_var( 's' );
		}

		if( get_query_var( 'orderby' ) ){
			$args['meta_key'] = ( get_query_var( 'orderby' ) ) ? get_query_var( 'orderby' ) : '';
			$args['orderby'] = ( get_query_var( 'orderby' ) ) ? 'meta_value' : '';
			$args['order'] = ( get_query_var( 'order' ) ) ? get_query_var( 'order' ) : 'ASC';
		}
		$args = apply_filters( 'fa_' . $this->post_type . '_get_posts', $this->custom_query( $args ) );
		global $fa_posts_query;
		$fa_posts_query = $args;
		foreach ( get_posts( $args ) as $key => $post ) {
			$class = $this->class;
			array_push( $return, new $class( $post->ID ) );
		}
		return $return;

	}

	protected function custom_query( $args ){

		return $args;

	}

}
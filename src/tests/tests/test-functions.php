<?php
/**
 * Class Test_FA_Functions
 *
 * @package Frontend_App
 */
class Test_FA_Functions extends WP_UnitTestCase {


	public function setUp(){

	    parent::setUp();

	}

	public function test_config() {

		// Check that the config file is an array
		$this->assertInternalType( 'array', fa_get_config() );
		$this->assertInternalType( 'array', fa_get_config( 'endpoints' ) );

		// Text domain should be 'text-domain'
		$this->assertEquals( fa_text_domain(), 'text-domain' );
	}

	public function test_fa_check_parameters(){

		// Check parameters
		$params = array(
			'name'
		);

		$data = json_decode( fa_check_parameters( $params ), true);
		$this->assertArrayHasKey('success', $data);

		$_POST['name'] = 'test';
		$this->assertNull( fa_check_parameters( $params ) );

	}

	public function test_fa_strip_querystring(){


		$url = fa_strip_querystring( 'http://example.test?sdf=test' );
		$parts = parse_url($url);
		$this->assertArrayNotHasKey( 'query', $parts );

	}

	public function test_fa_sort_url(){

		$_SERVER['HTTPS'] = 'https';
		$_SERVER["HTTP_HOST"] = str_replace( 'https://', '', constant( 'WP_HOME' ) );

		$url = fa_sort_url( 'title' );
		$this->assertEquals( $url, 'https://testsite.com?orderby=title&order=ASC' );

		$_GET['orderby'] = 'title';
		$_GET['order'] = 'ASC';
		$url = fa_sort_url( 'title' );
		$this->assertEquals( $url, 'https://testsite.com?orderby=title&order=DESC' );

		$url = fa_sort_url( 'id' );
		$this->assertEquals( $url, 'https://testsite.com?orderby=id&order=ASC');

	}
	
}
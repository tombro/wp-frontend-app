<?php
/**
 * Class Post_Type_Single_Test
 *
 * @package Frontend_App
 */
class Post_Type_Single_Test extends WP_UnitTestCase {

	private $dummy_post_type;
	private $post_id;
	private $meta_key = 'key';
	private $meta_value = 'value';

	public function setUp(){

		$this->post_id = $this->factory->post->create( array( 
			'post_type' => 'test',
			'post_title' => 'Title',
			'post_content' => 'Content'
		));

		update_post_meta( $this->post_id, $this->meta_key, $this->meta_value );
		$this->dummy_post_type = new Dummy_Post_Type_Single( $this->post_id );

	}

	public function test_get_id(){

		$this->assertEquals( $this->dummy_post_type->get_ID(), $this->post_id );

	}

	public function test_get_post_type(){

		$this->assertEquals( $this->dummy_post_type->get_post_type(), 'test' );

	}

	public function test_get_content(){

		$this->assertEquals( $this->dummy_post_type->get_content(), 'Content' );

	}

	public function test_get_name(){

		$this->assertEquals( $this->dummy_post_type->get_name(), 'Title' );

	}

	public function test_get_url(){

		$this->assertEquals( $this->dummy_post_type->get_url(), 'https://testsite.com/tests/edit/' . $this->post_id );

	}

	public function test_get(){

		$this->assertEquals( $this->dummy_post_type->get( $this->meta_key ), $this->meta_value );

	}

	public function test_set(){

		$new_meta_value = 'value2';
		$this->dummy_post_type->set( $this->meta_key, $new_meta_value );
		$this->assertEquals( $this->dummy_post_type->get( $this->meta_key ), $new_meta_value );

	}

	public function test_upsert(){

		$fields = array(
			'key' => 'value',
			'newkey' => 'value',
		);

		// Test insert
		$args = array(
			'title' => 'Test title',
			'meta' 	=> $fields
		);
		$dummy = new Dummy_Post_Type_Single();
		$dummy->upsert( $args );

		$this->assertTrue( is_numeric( $dummy->get_ID() ) );

		$args = array(
			'post_type' => 'test',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'newkey',
					'value' => 'value'
				)
			)
		);
		$this->assertEquals( 1, count( get_posts( $args ) ) );

		$fields = array(
			'newerkey' => 'value'
		);

		// Test update
		$args = array(
			'id' => $dummy->get_ID(),
			'title' => 'Test title',
			'meta' 	=> $fields
		);
		$dummy2 = new Dummy_Post_Type_Single();
		$dummy2->upsert( $args );

		$args = array(
			'post_type' => 'test',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'newkey',
					'value' => 'value'
				)
			)
		);
		$this->assertEquals( 1, count( get_posts( $args ) ) );
		$this->assertEquals( 'value', $dummy2->get( 'newkey' ) );
		$this->assertEquals( 'value', $dummy2->get( 'newerkey' ) );
		$this->assertEquals( '', $dummy2->get( 'newestkey' ) );

	}

}
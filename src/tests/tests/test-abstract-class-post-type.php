<?php
/**
 * Class Post_Type_Test
 *
 * @package Frontend_App
 */
class Post_Type_Test extends WP_UnitTestCase {

	private $post_titles;
	private $dummy_post_type;

	public function setUp(){

	    parent::setUp();

	    $this->post_titles = array(
	    	'first',
	    	'second',
	    	'third'
	    );

	    $this->dummy_post_type = new Dummy_Post_Type();
	    foreach ( $this->post_titles as $key => $post_title ) {
	    	$this->factory->post->create( array( 
				'post_type' => 'test',
				'post_title' => $post_title
			));
	    }
	}

	public function test_get() {

		$posts = $this->dummy_post_type->get();
		$this->assertInternalType( 'array', $posts );
		$this->assertEquals( count( $posts ), count( $this->post_titles ) );
		foreach ( $posts as $key => $post ) {
			$this->assertInstanceOf( 'Dummy_Post_Type_Single', $post );
		}

		// Search specific test object with title 'first'
		set_query_var( 's', 'first' );
		$search = $this->dummy_post_type->get();
		$this->assertEquals( count( $search ), 1 );

	}

	public function test_instance() {

		$instance = $this->dummy_post_type->instance();
		$this->assertInstanceOf( 'FA_Post_Type', $instance );

	}

}

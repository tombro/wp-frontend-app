<?php
/**
 * PHPUnit bootstrap file
 *
 * @package frontend-app
 */


$_tests_dir = getenv( 'WP_TESTS_DIR' );

if ( ! $_tests_dir ) {
	$_tests_dir = rtrim( sys_get_temp_dir(), '/\\' ) . '/wordpress-tests-lib';
}

if ( ! file_exists( $_tests_dir . '/includes/functions.php' ) ) {
	echo "Could not find $_tests_dir/includes/functions.php, have you run bin/install-wp-tests.sh ?" . PHP_EOL;
	exit( 1 );
}

// Give access to tests_add_filter() function.
require_once $_tests_dir . '/includes/functions.php';

/**
 * Manually load the plugin being tested.
 */
function _manually_load_plugin() {
	require_once dirname( dirname( __FILE__ ) ) . '/frontend-app.php';
	require_once dirname( dirname( __FILE__ ) ) . '/includes/class-frontend-app.php';
	require_once dirname( dirname( __FILE__ ) ) . '/tests/includes/class-test-app.php';
	require_once dirname( dirname( __FILE__ ) ) . '/tests/includes/class-dummy-post-type-single.php';
	require_once dirname( dirname( __FILE__ ) ) . '/tests/includes/class-dummy-post-type.php';
}
tests_add_filter( 'muplugins_loaded', '_manually_load_plugin' );

// Start up the WP testing environment.
require $_tests_dir . '/includes/bootstrap.php';

// Site variables
define( 'WP_HOME', 'https://testsite.com' );

// We need access to FA_CONFIG, it is loaded via our child class FA_Test
new FA_Test();
<?php
return array(
	'templates' => dirname(__FILE__) . '/templates',
	'endpoints' => array(
		'tests' => array(
			'slug' => 'test',
			'singular_slug' => __( 'test', 'text-domain' ),
			'plural_slug' => __( 'tests', 'text-domain' ),
			'singular_label' => __( 'Test', 'text-domain' ),
			'plural_label' => __( 'Tests', 'text-domain' ),
			'cap' => 'edit_posts',
			'template' => 'test/test',
			'children' => array(
				'new' => array(
					'slug' => __( 'new', 'text-domain' ),
					'label' => __( 'New', 'text-domain' ),
					'scope' => 'new',
					'cap' => 'edit_posts'
				),
				'edit' => array(
					'slug' => __( 'edit', 'text-domain' ),
					'label' => __( 'Edit', 'text-domain' ),
					'scope' => 'edit',
					'cap' => 'edit_posts'
				),
			),
			'classes' =>  array(
				'group' => 'Tests',
				'single' => 'Test',
			),
			'cpt' => array(
				'show_in_menu' => get_current_user_id() == 1
			)
		),
	),
	'post_types' => array(
		'tester' => array(
			'singular_label' => __( 'Tester', 'text-domain' ),
			'plural_label' => __( 'Tester', 'text-domain' ),
			'options' => array(
				'menu_icon' => 'dashicons-groups',
				'supports' => array( 'title' ),
				'show_in_menu' => get_current_user_id() == 1
			),
		)
	),
	'taxonomies' => array(
		'custom_tax' => array(
			'singular_label' => __( 'Custom tax', 'text-domain' ),
			'plural_label' => __( 'Custom tax', 'text-domain' ),
			'post_type' => 'tester',
			'options' => array()
		)
	),
	'i18n' => array(
		'text-domain' => 'text-domain',
		'strings' => array(
		)
	)
);
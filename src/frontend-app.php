<?php
class FA{

	protected static $_instance = null;
	public $text_domain;

	public function __construct(){
		if( $this->setUpConfig() ){

			$this->includes();

			// Set text domain from parent class
			$i18n = fa_get_config( 'i18n' );
			$this->text_domain = $i18n['text-domain'];
			
			add_action( 'init', array( $this, 'generate_routes' ), 99 );
			add_action( 'init', array( $this, 'generate_cpts' ), 99 );

			add_filter( 'query_vars', array( $this, 'custom_query_vars' ) );
			add_filter( 'template_include', array( $this, 'custom_template_include' ) );

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 9999 );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 9999 );
			add_action( 'pre_get_posts', array( $this, 'title_or_meta_query' ) );

		}	

	}

	public function includes(){

		include 'includes/abstract/abstract-class-post-type.php';
		include 'includes/abstract/abstract-class-post-type-single.php';

		include 'includes/helpers/functions.php';
		include 'includes/helpers/assets.php';

		include 'includes/class-frontend.php';
		include 'includes/class-paginator.php';
		include 'includes/class-admin.php';

	}

	public function generate_routes() {

		foreach ( fa_get_config( 'endpoints' ) as $name => $endpoint ) {

			if( isset( $endpoint['children'] ) ){

				foreach ( $endpoint['children'] as $key => $child) {

					if( isset( $child['scope'] ) ){

						switch ( $child['scope'] ) {

							case 'edit':
							add_rewrite_rule('^' . $endpoint['plural_slug'] . '/([0-9]+)/' . $child['slug'] . '/?$', 'index.php?custompage=' . $name . '&custompage_scope=edit&custompage_id=$matches[1]', 'top');
							break;

							case 'new':
							add_rewrite_rule('^' . $endpoint['plural_slug'] . '/' . $child['slug'] . '/?$', 'index.php?custompage=' . $name . '&custompage_scope=new', 'top');
							break;
							
							default:
							add_rewrite_rule('^' . $endpoint['plural_slug'] . '/' . $child['slug'] . '/?$', 'index.php?custompage=' . $name . '&custompage_scope=' . $key, 'top');
							break;

						}
					}
					else{

						add_rewrite_rule('^' . $endpoint['plural_slug'] . '/' . $child['slug'] . '/?$', 'index.php?custompage=' . $name . '&custompage_scope=' . $key, 'top');
					}
				}
			}
			add_rewrite_rule('^' . $endpoint['plural_slug'] . '/?$', 'index.php?custompage=' . $name, 'top');

		}

	}

	public function generate_cpts(){

		$endpoints = fa_get_config( 'endpoints' );
		$post_types = fa_get_config( 'post_types' );
		$taxonomies = fa_get_config( 'taxonomies' );

		if( $endpoints ){
			foreach ( fa_get_config( 'endpoints' ) as $name => $endpoint ) {

				if( isset( $endpoint['cpt'] ) ){

					$this->add_post_type( $endpoint['slug'], $endpoint['singular_label'], $endpoint['plural_label'], $endpoint['cpt'] );

				}

			}
		}
		if( $post_types ){
			foreach ( $post_types as $name => $post_type ){

				$this->add_post_type( $name, $post_type['singular_label'], $post_type['plural_label'], $post_type['options'] );

			}
		}
		if( $taxonomies ){
			foreach ( $taxonomies as $name => $taxonomy ){

				$this->add_taxonomy( $name, $taxonomy['singular_label'], $taxonomy['plural_label'], $taxonomy['post_type'], $taxonomy['options'] );

			}
		}

	}

	public function add_post_type( $name, $singular_name, $plural_name, $options = array() ){

		$labels = array(
			'name'               => ucfirst( $plural_name ),
			'singular_name'      => ucfirst( $singular_name ),
			'menu_name'          => ucfirst( $plural_name ),
			'name_admin_bar'     => ucfirst( $singular_name ),
			'add_new'            => __( 'Add new', 'fa' ),
			'add_new_item'       => sprintf( __( 'Add new %s', 'fa' ), $singular_name ),
			'new_item'           => sprintf( __( 'New %s', 'fa' ), $singular_name ),
			'edit_item'          => sprintf( __( 'Edit %s', 'fa' ), $singular_name ),
			'view_item'          => sprintf( __( 'View %s', 'fa' ), $singular_name ),
			'all_items'          => sprintf( __( 'All %s', 'fa' ), $plural_name ),
			'search_items'       => sprintf( __( 'Search %s', 'fa' ), $plural_name ),
			'parent_item_colon'  => sprintf( __( 'Parent %s', 'fa' ), $singular_name ),
			'not_found'          => sprintf( __( 'No %s found', 'fa' ), $plural_name ),
			'not_found_in_trash' => sprintf( __( 'No %s found in trash', 'fa' ), $plural_name )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $name ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'exclude_from_search'=> true,
			'menu_position'      => null,
			'supports'           => ( isset( $options['supports'] ) ? $options['supports'] : array( 'title', 'author', 'thumbnail' ) ),
		);

		foreach ($options as $key => $value) {
			$args[$key] = $value;
		}

		register_post_type( $name, $args );

	}

	public function add_taxonomy( $name, $singular_name, $plural_name, $post_type = array(), $options = array() ){

		$labels = array(
			'name'              => ucfirst( $plural_name ),
			'singular_name'     => ucfirst( $singular_name ),
			'search_items'      => sprintf( __( 'Search %s', 'fa' ), $plural_name ),
			'all_items'         => sprintf( __( 'All %s', 'fa' ), $plural_name ),
			'parent_item'       => sprintf( __( 'Parent %s', 'fa' ), $singular_name ),
			'parent_item_colon' => sprintf( __( 'Parent %s:', 'fa' ), $singular_name ),
			'edit_item'         => sprintf( __( 'Edit %s', 'fa' ), $singular_name ),
			'update_item'       => sprintf( __( 'Update %s', 'fa' ), $singular_name ),
			'add_new_item'      => sprintf( __( 'Add new %s', 'fa' ), $singular_name ),
			'new_item_name'     => sprintf( __( 'New %s', 'fa' ), $singular_name ),
			'menu_name'         => ucfirst( $plural_name )
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => $name ),
		);

		foreach ($options as $key => $value) {
			$args[$key] = $value;
		}

		register_taxonomy( $name, $post_type, $args );

	}

	public function custom_query_vars( $vars ) {

		$vars[] = 'custompage';
		$vars[] = 'custompage_id';
		$vars[] = 'custompage_scope';
		return $vars;

	}

	public function custom_template_include( $template ) {
		
		global $wp_query;
		$new_template = '';

		// If url contains the custompage arg
		if ( isset( $wp_query->query_vars['custompage'] ) ) {
			$endpoints = fa_get_config( 'endpoints' );
			$endpoint = $endpoints[$wp_query->query_vars['custompage']];

			if( isset( $wp_query->query_vars['custompage_scope']) ){
				$cap = 'manage_options';
				foreach ($endpoint['children'] as $key => $child) {
					if( isset( $child['scope'] ) && $child['scope'] == $wp_query->query_vars['custompage_scope'] ){
						$cap = $child['cap'];
					}
					else{
						$cap = $endpoint['cap'];
					}
				}
				if( !current_user_can( $cap ) ){
					auth_redirect();
				}
			}
			if( !current_user_can( $endpoint['cap'] ) ){
				auth_redirect();
			}

			$newtemplate = '';
			if( isset( $endpoint['template'] ) ){
				$newtemplate = rtrim( fa_get_config( 'templates' ), '/' ) . '/' . $endpoint['template'];
			}
			else{
				$newtemplate = plugin_dir_path( __FILE__ ) .  'template-not-found';
			}

			// Set up template
			if( isset( $wp_query->query_vars['custompage_scope'] ) ){
				$newtemplate .= '-' . $wp_query->query_vars['custompage_scope'];
			}
			// Set up endpoint variable eg. global $client or $student
			if( isset( $wp_query->query_vars['custompage_id'] ) ){
				global $page_object;
				$page_object = $wp_query->query_vars['custompage_id'];
				foreach ( $endpoints as $key => $endpoint ) {
					if( isset( $endpoint['classes'] ) && $wp_query->query_vars['custompage'] == $key ){

						if( $endpoint['slug'] == get_post_type( $wp_query->query_vars['custompage_id'] ) ){

							global $queried_post_type;
							$queried_post_type = $endpoint['slug'];

							global ${$endpoint['slug']};
							${$endpoint['slug']} = new $endpoint['classes']['single']( $wp_query->query_vars['custompage_id'] );

						}
						else{

							$newtemplate = plugin_dir_path( __FILE__ ) . 'templates/post-type-mismatch';
							
						}
					}
				}
			}
			elseif( !isset( $wp_query->query_vars['custompage_id'] ) && !isset( $wp_query->query_vars['custompage_scope'] ) ){
				foreach ( $endpoints as $key => $endpoint ) {
					if( isset( $endpoint['classes'] ) && $wp_query->query_vars['custompage'] == $key ){

						global ${$key};
						$class = new $endpoint['classes']['group']();
						${$key} = $class->get();
						
						global $queried_post_type;
						$queried_post_type = $endpoint['slug'];

						global $queried_post_type_class;
						$queried_post_type_class = $class;

					}
				}
			}
			$newtemplate = $newtemplate  . '.php';
			if( file_exists( $newtemplate ) ){
				$template = $newtemplate;
			}
			else{
				global $requested_template;
				$requested_template = $newtemplate;
				$template = plugin_dir_path( __FILE__ ) .  'templates/template-not-found.php';
			}
			
		}
		return $template;
	}

	public function enqueue_scripts(){

		// jQuery Validation
		wp_enqueue_script( 'jquery-validation', '//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', array( 'jquery' ), '1.17.0', true  );

        // Simple jQuery Datepicker
        wp_enqueue_style('datepicker', '//cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css', null, true);
        wp_enqueue_script('datepicker', '//cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js', array( 'jquery' ), null, true);

		// Chosen
		wp_enqueue_script( 'chosen', '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js', array( 'jquery' ), '1.8.7', true  );
		wp_enqueue_style( 'chosen', '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css', null, '1.8.7');

		//Inputmask
		wp_enqueue_script( 'inputmask', '//cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js', array( 'jquery' ), '3.3.4', true );

		// Custom JS & CSS
		wp_enqueue_style( 'frontend-app', fa_asset_path( 'dist/css/main.css' ) );
		wp_enqueue_script( 'frontend-app', fa_asset_path( 'dist/js/main.js' ), array( 'jquery', 'jquery-validation', 'chosen', 'datepicker', 'inputmask' ), null, true );

	}

	public function admin_enqueue_scripts(){

		wp_enqueue_style( 'frontend-app', plugins_url( '../public/css/frontend-app-admin.css', __FILE__ ) );

	}

	public function title_or_meta_query( $q ){

		if( $title = $q->get( '_meta_or_title' ) ){
			add_filter( 'get_meta_sql', function( $sql ) use ( $title ){
				global $wpdb;

				static $nr = 0; 
				if( 0 != $nr++ ) return $sql;

				$sql['where'] = sprintf(
					" AND ( %s OR %s ) ",
					$wpdb->prepare( "{$wpdb->posts}.post_title LIKE '%%%s%%'", $title ),
					mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
				);
				return $sql;
			});
		}

	}

	private function setUpConfig(){

		$reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
		$rootDir = dirname(dirname(dirname($reflection->getFileName())));
		$configFile = $rootDir . '/config.php';

		if( !file_exists($configFile) ){
			?>
			<h1><?php _e( 'Configuration file not found', 'frontend-app' ); ?></h1>
			<pre><?php echo $configFile; ?></pre>
			<?php
			return false;
		}
		$config 	= include( $configFile );
		// Check for errors in the config file
		$errors = $this->check_config( $config );
		if( !empty( $errors ) ){
			?>
			<h1><?php _e( 'Invalid configuration file', 'frontend-app' ); ?></h1>
			<pre>
				<?php print_r( $errors ); ?>
			</pre>
			<?php
			return false;
		}
		else{
			// Store config in constant
			if( !defined( 'FA_CONFIG' ) ){
				define( 'FA_CONFIG', serialize( $config ) );
			}
			return true;
		}

	}

	private function check_config( $config ){

		$errors = array();
		$home_count = 0;
		if( isset( $config['endpoints'] ) ){
			foreach ( $config['endpoints'] as $name => $endpoint ) {
				$required = array( 'cap', 'template' );
				foreach ($required as $required_item) {
					if( !isset( $endpoint[$required_item] ) ){
						array_push( $errors, sprintf( '%s\'s %s are not set', $name, $required_item ) );
					}
				}
				if( isset( $endpoint['is_home'] ) && ++$home_count > 1 ){

					$errors[] = __( 'There are multiple endpoints set as home', 'frontend-app' );

				}
			}
		}
		else{
			$errors[] = __( 'Endpoints are not defined', 'frontend-app' );
		}
		if( !isset( $config['post_types'] ) ){
			$errors[] = __( 'post_types is not defined', 'frontend-app' );
		}
		if( !isset( $config['i18n'] ) ){
			$errors[] = __( 'i18n is not defined', 'frontend-app' );
		}
		if( !isset( $config['i18n']['text-domain'] )  ){
			$errors[] = __( 'i18n > text-domain is not defined', 'frontend-app' );
		}
		return $errors;

	}

	public static function instance()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

}